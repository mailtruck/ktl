package main

type APIsResponse struct {
	Total int    `json:"total"`
	Data  []Data `json:"data"`
}
type Methods struct {
}
type Data struct {
URIs interface{} `json:"uris"`
	CreatedAt              int64         `json:"created_at"`
	StripURI               bool          `json:"strip_uri"`
	ID                     string        `json:"id"`
	Hosts                  []string      `json:"hosts"`
	Name                   string        `json:"name"`
	Methods                Methods       `json:"methods,omitempty"`
	HTTPIfTerminated       bool          `json:"http_if_terminated"`
	HTTPSOnly              bool          `json:"https_only"`
	Retries                int           `json:"retries"`
	PreserveHost           bool          `json:"preserve_host"`
	UpstreamConnectTimeout int           `json:"upstream_connect_timeout"`
	UpstreamReadTimeout    int           `json:"upstream_read_timeout"`
	UpstreamSendTimeout    int           `json:"upstream_send_timeout"`
	UpstreamURL            string        `json:"upstream_url"`
}
