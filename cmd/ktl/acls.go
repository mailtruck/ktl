package main

type ACLsResp struct {
	Total int `json:"total"`
	Data  []struct {
		Group      string `json:"group"`
		CreatedAt  int64  `json:"created_at"`
		ID         string `json:"id"`
		ConsumerID string `json:"consumer_id"`
	} `json:"data"`
}
