package main

type PluginsResp struct {
	Total int `json:"total"`
	Data  []struct {
		CreatedAt int64 `json:"created_at"`
		ID      string `json:"id"`
		Name    string `json:"name"`
		APIID   string `json:"api_id"`
		Enabled bool   `json:"enabled"`
		Config  struct {
Origins interface {} `json:"origins"`
			Whitelist []string `json:"whitelist"`
			Remove struct {
				Querystring interface {} `json:"querystring"`
				Headers interface{}  `json:"headers"`
				Body interface{}  `json:"body"`
				Json interface{} `json:"json"`
			} `json:"remove"`
			Append struct {
				Querystring interface { } `json:"querystring"`
				Headers interface { } `json:"headers"`
				Body interface{ } `json:"body"`
			} `json:"append"`
			Replace struct {
				Querystring interface { } `json:"querystring"`
				Headers interface { } `json:"headers"`
				Body interface { } `json:"body"`
			} `json:"replace"`
			Add struct {
				Querystring interface{} `json:"querystring"`
				Headers    interface { } `json:"headers"`
				Body interface{} `json:"body"`
			} `json:"add"`
			Rename struct {
				Querystring interface{}  `json:"querystring"`
				Headers interface{} `json:"headers"`
				Body interface{}  `json:"body"`
			} `json:"rename"`
			KeyNames        []string `json:"key_names"`
			Anonymous       string   `json:"anonymous"`
			RunOnPreflight  bool     `json:"run_on_preflight"`
			HideCredentials bool     `json:"hide_credentials"`
		} `json:"config,omitempty"`
	} `json:"data"`
}
