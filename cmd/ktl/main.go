package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

func main() {

	http := &http.Client{
		Timeout: time.Second * 10,
	}

	if len(os.Args) > 1 && os.Args[1] == "consumers" {
		resp, err := http.Get("http://localhost:8001/consumers")
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		consumers := ConsumersResp{}
		err = json.NewDecoder(resp.Body).Decode(&consumers)
		if err != nil {
			panic(err)
		}

		for _, consumer := range consumers.Data {
			fmt.Println(consumer.Username)
			resp, err := http.Get(fmt.Sprintf("http://localhost:8001/consumers/%v/key-auth", consumer.Username))
			if err != nil {
				panic(err)
			}
			keyAuth := KeyAuthResp{}
			defer resp.Body.Close()
			err = json.NewDecoder(resp.Body).Decode(&keyAuth)
			if err != nil {
				panic(err)

			}
			for _, key := range keyAuth.Data {
				fmt.Println("key-auth")
				fmt.Println(key.Key)
			}

			resp, err = http.Get(fmt.Sprintf("http://localhost:8001/consumers/%v/acls", consumer.Username))
			if err != nil {
				panic(err)
			}
			defer resp.Body.Close()
			acls := ACLsResp{}
			err = json.NewDecoder(resp.Body).Decode(&acls)
			if err != nil {
				panic(err)
			}

			fmt.Println("acls")
			for _, acl := range acls.Data {
				fmt.Println(acl.Group)
			}
			fmt.Println("")
		}

		return

	}

	resp, err := http.Get("http://localhost:8001/apis")
	if err != nil {
		panic(err)
	}

	apis := &APIsResponse{}

	err = json.NewDecoder(resp.Body).Decode(apis)
	defer resp.Body.Close()
	if err != nil {
		panic(err)
	}

	for _, api := range apis.Data {
		fmt.Println(fmt.Sprintf("name: %v", api.Name))
		fmt.Println(fmt.Sprintf("hosts: %v", api.Hosts))
		fmt.Println(fmt.Sprintf("upstream: %v", api.UpstreamURL))
		fmt.Println(fmt.Sprintf("uris: %v", api.URIs))

		resp, err := http.Get(fmt.Sprintf("http://localhost:8001/apis/%v/plugins", api.Name))
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		plugins := PluginsResp{}
		err = json.NewDecoder(resp.Body).Decode(&plugins)
		if err != nil {
			panic(err)
		}

		for _, plugin := range plugins.Data {
			fmt.Println(plugin.Name)
			switch plugin.Name {
			case "acl":
				fmt.Println(fmt.Sprintf("whitelist: %v", plugin.Config.Whitelist))
			case "request-transformer":
				fmt.Println(plugin.Config.Add.Querystring)
			case "key-auth":
				fmt.Println(plugin.Config.KeyNames)
			case "response-transformer":
				fmt.Println(plugin.Config.Remove.Json)
			case "cors":
				fmt.Println(plugin.Config.Origins)
			case "basic-auth":
				fmt.Println("true")
			default:
				panic(plugin.Name)
			}
		}

		fmt.Println("")

	}

}
