package main

type KeyAuthResp struct {
	Total int `json:"total"`
	Data  []struct {
		ID         string `json:"id"`
		CreatedAt  int64  `json:"created_at"`
		Key        string `json:"key"`
		ConsumerID string `json:"consumer_id"`
	} `json:"data"`
}
