package main

type ConsumersResp struct {
	Next interface{} `json:"next"`
	Data []struct {
		CustomID  interface{} `json:"custom_id"`
		CreatedAt int         `json:"created_at"`
		Username  string      `json:"username"`
		ID        string      `json:"id"`
	} `json:"data"`
}
