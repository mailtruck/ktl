package main

type AdminResp struct {
	Plugins       Plugins       `json:"plugins"`
	Tagline       string        `json:"tagline"`
	Configuration Configuration `json:"configuration"`
	Version       string        `json:"version"`
	NodeID        string        `json:"node_id"`
	LuaVersion    string        `json:"lua_version"`
	PrngSeeds     PrngSeeds     `json:"prng_seeds"`
	Timers        Timers        `json:"timers"`
	Hostname      string        `json:"hostname"`
}
type AvailableOnServer struct {
	ResponseTransformer  bool `json:"response-transformer"`
	Oauth2               bool `json:"oauth2"`
	ACL                  bool `json:"acl"`
	CorrelationID        bool `json:"correlation-id"`
	PreFunction          bool `json:"pre-function"`
	Jwt                  bool `json:"jwt"`
	Cors                 bool `json:"cors"`
	IPRestriction        bool `json:"ip-restriction"`
	BasicAuth            bool `json:"basic-auth"`
	KeyAuth              bool `json:"key-auth"`
	RateLimiting         bool `json:"rate-limiting"`
	RequestTransformer   bool `json:"request-transformer"`
	HTTPLog              bool `json:"http-log"`
	FileLog              bool `json:"file-log"`
	HmacAuth             bool `json:"hmac-auth"`
	LdapAuth             bool `json:"ldap-auth"`
	Datadog              bool `json:"datadog"`
	TCPLog               bool `json:"tcp-log"`
	Zipkin               bool `json:"zipkin"`
	PostFunction         bool `json:"post-function"`
	RequestSizeLimiting  bool `json:"request-size-limiting"`
	BotDetection         bool `json:"bot-detection"`
	Syslog               bool `json:"syslog"`
	Loggly               bool `json:"loggly"`
	AzureFunctions       bool `json:"azure-functions"`
	UDPLog               bool `json:"udp-log"`
	ResponseRatelimiting bool `json:"response-ratelimiting"`
	AwsLambda            bool `json:"aws-lambda"`
	Statsd               bool `json:"statsd"`
	Prometheus           bool `json:"prometheus"`
	RequestTermination   bool `json:"request-termination"`
}
type Plugins struct {
	EnabledInCluster  []string          `json:"enabled_in_cluster"`
	AvailableOnServer AvailableOnServer `json:"available_on_server"`
}
type TrustedIps struct {
}
type LoadedPlugins struct {
	ResponseTransformer  bool `json:"response-transformer"`
	RequestTermination   bool `json:"request-termination"`
	Prometheus           bool `json:"prometheus"`
	IPRestriction        bool `json:"ip-restriction"`
	PreFunction          bool `json:"pre-function"`
	Jwt                  bool `json:"jwt"`
	Cors                 bool `json:"cors"`
	Statsd               bool `json:"statsd"`
	BasicAuth            bool `json:"basic-auth"`
	KeyAuth              bool `json:"key-auth"`
	LdapAuth             bool `json:"ldap-auth"`
	AwsLambda            bool `json:"aws-lambda"`
	HTTPLog              bool `json:"http-log"`
	ResponseRatelimiting bool `json:"response-ratelimiting"`
	HmacAuth             bool `json:"hmac-auth"`
	RequestSizeLimiting  bool `json:"request-size-limiting"`
	Datadog              bool `json:"datadog"`
	TCPLog               bool `json:"tcp-log"`
	Zipkin               bool `json:"zipkin"`
	PostFunction         bool `json:"post-function"`
	BotDetection         bool `json:"bot-detection"`
	ACL                  bool `json:"acl"`
	Loggly               bool `json:"loggly"`
	Syslog               bool `json:"syslog"`
	AzureFunctions       bool `json:"azure-functions"`
	UDPLog               bool `json:"udp-log"`
	FileLog              bool `json:"file-log"`
	RequestTransformer   bool `json:"request-transformer"`
	CorrelationID        bool `json:"correlation-id"`
	RateLimiting         bool `json:"rate-limiting"`
	Oauth2               bool `json:"oauth2"`
}
type DNSResolver struct {
}
type NginxAdminDirectives struct {
}
type CustomPlugins struct {
}
type ProxyListeners struct {
	Ssl           bool   `json:"ssl"`
	IP            string `json:"ip"`
	ProxyProtocol bool   `json:"proxy_protocol"`
	Port          int    `json:"port"`
	HTTP2         bool   `json:"http2"`
	Listener      string `json:"listener"`
}
type EnabledHeaders struct {
	LatencyTokens        bool `json:"latency_tokens"`
	XKongProxyLatency    bool `json:"X-Kong-Proxy-Latency"`
	Via                  bool `json:"Via"`
	ServerTokens         bool `json:"server_tokens"`
	Server               bool `json:"Server"`
	XKongUpstreamLatency bool `json:"X-Kong-Upstream-Latency"`
	XKongUpstreamStatus  bool `json:"X-Kong-Upstream-Status"`
}
type AdminListeners struct {
	Ssl           bool   `json:"ssl"`
	IP            string `json:"ip"`
	ProxyProtocol bool   `json:"proxy_protocol"`
	Port          int    `json:"port"`
	HTTP2         bool   `json:"http2"`
	Listener      string `json:"listener"`
}
type NginxProxyDirectives struct {
}
type NginxHTTPDirectives struct {
	Value string `json:"value"`
	Name  string `json:"name"`
}
type Configuration struct {
	Plugins                         []string              `json:"plugins"`
	AdminSslEnabled                 bool                  `json:"admin_ssl_enabled"`
	LuaSslVerifyDepth               int                   `json:"lua_ssl_verify_depth"`
	TrustedIps                      TrustedIps            `json:"trusted_ips"`
	Prefix                          string                `json:"prefix"`
	LoadedPlugins                   LoadedPlugins         `json:"loaded_plugins"`
	CassandraUsername               string                `json:"cassandra_username"`
	AdminSslCertCsrDefault          string                `json:"admin_ssl_cert_csr_default"`
	SslCertKey                      string                `json:"ssl_cert_key"`
	AdminSslCertKey                 string                `json:"admin_ssl_cert_key"`
	DNSResolver                     DNSResolver           `json:"dns_resolver"`
	PgUser                          string                `json:"pg_user"`
	MemCacheSize                    string                `json:"mem_cache_size"`
	CassandraDataCenters            []string              `json:"cassandra_data_centers"`
	NginxAdminDirectives            NginxAdminDirectives  `json:"nginx_admin_directives"`
	CustomPlugins                   CustomPlugins         `json:"custom_plugins"`
	PgHost                          string                `json:"pg_host"`
	NginxAccLogs                    string                `json:"nginx_acc_logs"`
	ProxyListen                     []string              `json:"proxy_listen"`
	ClientSslCertDefault            string                `json:"client_ssl_cert_default"`
	SslCertKeyDefault               string                `json:"ssl_cert_key_default"`
	DNSNoSync                       bool                  `json:"dns_no_sync"`
	DbUpdatePropagation             int                   `json:"db_update_propagation"`
	NginxErrLogs                    string                `json:"nginx_err_logs"`
	CassandraPort                   int                   `json:"cassandra_port"`
	DNSOrder                        []string              `json:"dns_order"`
	DNSErrorTTL                     int                   `json:"dns_error_ttl"`
	Headers                         []string              `json:"headers"`
	DNSStaleTTL                     int                   `json:"dns_stale_ttl"`
	NginxOptimizations              bool                  `json:"nginx_optimizations"`
	Database                        string                `json:"database"`
	PgDatabase                      string                `json:"pg_database"`
	NginxWorkerProcesses            string                `json:"nginx_worker_processes"`
	LuaPackageCpath                 string                `json:"lua_package_cpath"`
	AdminAccLogs                    string                `json:"admin_acc_logs"`
	LuaPackagePath                  string                `json:"lua_package_path"`
	NginxPid                        string                `json:"nginx_pid"`
	UpstreamKeepalive               int                   `json:"upstream_keepalive"`
	CassandraContactPoints          []string              `json:"cassandra_contact_points"`
	AdminAccessLog                  string                `json:"admin_access_log"`
	ClientSslCertCsrDefault         string                `json:"client_ssl_cert_csr_default"`
	ProxyListeners                  []ProxyListeners      `json:"proxy_listeners"`
	ProxySslEnabled                 bool                  `json:"proxy_ssl_enabled"`
	PgPassword                      string                `json:"pg_password"`
	CassandraSsl                    bool                  `json:"cassandra_ssl"`
	EnabledHeaders                  EnabledHeaders        `json:"enabled_headers"`
	SslCertCsrDefault               string                `json:"ssl_cert_csr_default"`
	ClientSsl                       bool                  `json:"client_ssl"`
	DbResurrectTTL                  int                   `json:"db_resurrect_ttl"`
	ErrorDefaultType                string                `json:"error_default_type"`
	CassandraConsistency            string                `json:"cassandra_consistency"`
	ClientMaxBodySize               string                `json:"client_max_body_size"`
	AdminErrorLog                   string                `json:"admin_error_log"`
	PgSslVerify                     bool                  `json:"pg_ssl_verify"`
	DNSNotFoundTTL                  int                   `json:"dns_not_found_ttl"`
	PgSsl                           bool                  `json:"pg_ssl"`
	DbUpdateFrequency               int                   `json:"db_update_frequency"`
	SslCiphers                      string                `json:"ssl_ciphers"`
	CassandraReplStrategy           string                `json:"cassandra_repl_strategy"`
	CassandraReplFactor             int                   `json:"cassandra_repl_factor"`
	LogLevel                        string                `json:"log_level"`
	AdminSslCert                    string                `json:"admin_ssl_cert"`
	RealIPHeader                    string                `json:"real_ip_header"`
	KongEnv                         string                `json:"kong_env"`
	CassandraSchemaConsensusTimeout int                   `json:"cassandra_schema_consensus_timeout"`
	DNSHostsfile                    string                `json:"dns_hostsfile"`
	AdminListeners                  []AdminListeners      `json:"admin_listeners"`
	CassandraTimeout                int                   `json:"cassandra_timeout"`
	SslCert                         string                `json:"ssl_cert"`
	ProxyAccessLog                  string                `json:"proxy_access_log"`
	AdminSslCertKeyDefault          string                `json:"admin_ssl_cert_key_default"`
	CassandraSslVerify              bool                  `json:"cassandra_ssl_verify"`
	SslCipherSuite                  string                `json:"ssl_cipher_suite"`
	CassandraLbPolicy               string                `json:"cassandra_lb_policy"`
	RealIPRecursive                 string                `json:"real_ip_recursive"`
	ProxyErrorLog                   string                `json:"proxy_error_log"`
	ClientSslCertKeyDefault         string                `json:"client_ssl_cert_key_default"`
	NginxDaemon                     string                `json:"nginx_daemon"`
	AnonymousReports                bool                  `json:"anonymous_reports"`
	DbCacheTTL                      int                   `json:"db_cache_ttl"`
	NginxProxyDirectives            NginxProxyDirectives  `json:"nginx_proxy_directives"`
	PgPort                          int                   `json:"pg_port"`
	NginxKongConf                   string                `json:"nginx_kong_conf"`
	ClientBodyBufferSize            string                `json:"client_body_buffer_size"`
	LuaSocketPoolSize               int                   `json:"lua_socket_pool_size"`
	AdminSslCertDefault             string                `json:"admin_ssl_cert_default"`
	NginxHTTPDirectives             []NginxHTTPDirectives `json:"nginx_http_directives"`
	CassandraKeyspace               string                `json:"cassandra_keyspace"`
	SslCertDefault                  string                `json:"ssl_cert_default"`
	NginxConf                       string                `json:"nginx_conf"`
	AdminListen                     []string              `json:"admin_listen"`
}
type PrngSeeds struct {
	Pid58 int64 `json:"pid: 58"`
	Pid59 int64 `json:"pid: 59"`
}
type Timers struct {
	Pending int `json:"pending"`
	Running int `json:"running"`
}
